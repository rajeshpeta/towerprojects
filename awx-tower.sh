#!/bin/bash
# login as a root user and execute these commands
# sudo commands execute as a ubuntu user
# non sudo commands execute as a root user
sudo mkdir /tower
sudo apt-get update
cd /tower
sudo git clone https://github.com/ansible/awx.git
sudo apt-get install     apt-transport-https     ca-certificates     curl     gnupg-agent     software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
apt-cache madison docker-ce
sudo apt-get install docker-ce=<VERSION_STRING> docker-ce-cli=<VERSION_STRING> containerd.io
apt install python-pip
apt install python3-pip
sudo apt-get install ansible
pip install ansible
pip3 install ansible
pip install docker-compose
pip install docker
pip3 install docker
pip install docker-compose
pip3 install docker-compose
cd /tower/awx/installer
ansible-playbook -i inventory install.yml
sudo ufw allow 8080
sudo ufw allow OpenSSH
sudo ufw enable
sudo ufw status
